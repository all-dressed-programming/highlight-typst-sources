{
  inputs = {
    naersk.url = "github:nix-community/naersk/master";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        naersk-lib = pkgs.callPackage naersk { };
        PROJECT_ROOT = builtins.toString ./.;
        defaultPackage = naersk-lib.buildPackage ./.;
        defaultApp = utils.lib.mkApp {
          drv = self.defaultPackage."${system}";
        };

        development-environment = with pkgs; buildEnv {
            name = "IntelliJ development environment";
            paths = [
              # something that work.. ish
              rustc
              cargo
              rustfmt
              rustPlatform.rustcSrc
              gcc
              cargo-tarpaulin
              ];
        };
      in
      {

        packages = {
          development-environment = development-environment;
          default = defaultPackage;
        };

        devShell = with pkgs; mkShell {
          buildInputs = [cargo
                         rustc
                         rustfmt
                         pre-commit
                         rustPackages.clippy
                         cargo-tarpaulin
                         deno
                         foreman
                         postgresql];
          RUST_SRC_PATH = rustPlatform.rustLibSrc;
        };
      });
}
