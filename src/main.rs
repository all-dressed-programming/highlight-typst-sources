use typst::syntax::parse_code;
use std::io;
use std::io::Read;


fn main() -> io::Result<()> {
    let mut stdin = io::stdin(); // We get `Stdin` here.
    let mut buffer = Vec::new();
    stdin.read_to_end(&mut buffer)?;
    let input_string = std::str::from_utf8(& buffer).unwrap();

    let node = parse_code(input_string);
    let highlighted = typst::ide::highlight_html(&node);
    println!("{}", highlighted);
    Ok(())
}
